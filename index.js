const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const routerNavigation = require('./src/index');
const morgan = require('morgan');

app.listen(3001, "127.0.0.1", () => {
    console.log("Example app listening localhost:3001");
});

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', 'http://localhost:3000');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    next();
});

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(morgan("dev"));
app.use('/', routerNavigation);