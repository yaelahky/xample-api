const { createUser, loginUser } = require('../models/auth')
const helper = require('../helper')
const jwt = require('jsonwebtoken')
const nodemailer = require("nodemailer");

module.exports = {
    createUser: async(request, response) => {
        try {
            const newDate = new Date()
            const data = {
                name: request.body.name,
                username: request.body.username,
                password: request.body.password,
                created_at: newDate,
                updated_at: newDate
            }
            const result = await createUser(data)
            return helper.response(response, 200, result)
        } catch (error) {
            console.log(error)
            return helper.response(response, 400, error)
        }
    },
    loginUser: async(request, response) => {
        let transporter = nodemailer.createTransport({
            host: "smtp.gmail.com",
            port: 465,
            secure: true, // true for 465, false for other ports
            auth: {
              user: "testedmember3@gmail.com", // generated ethereal user
              pass: "MAKLO123" // generated ethereal password
            }
          });
        
          // send mail with defined transport object
          let info = await transporter.sendMail({
            from: '"BOSBIS 👻" <foo@example.com>', // sender address
            to: "anandarizkyyuliansyah@gmail.com", // list of receivers
            subject: "Hello Ada yang masuk ke akun anda ✔", // Subject line
            html: "<b>Login Berhasil?</b>" // html body
          });
        
          console.log("Message sent: %s", info.messageId);
        try {
            const userData = {
                username: request.body.username,
                password: request.body.password
            }
            const result = await loginUser(userData)
            const token = jwt.sign({result}, 'RAHASIA', {expiresIn: '1m'})
            return helper.response(response, 200, {token: token, username: request.body.username, name: "Ananda Rizky Yuliansyah"})
        } catch (error) {
            console.log(error)
            return helper.response(response, 400, error)
        }
    }
}