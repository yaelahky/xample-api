const express = require('express');
const Route = express.Router();

const {getProducts, createProducts, editProducts, deleteProducts} = require('../controller/products');
const {authorization} = require('../middleware/auth')

Route
    .get('/', getProducts)
    .post('/', createProducts)
    .patch('/:id', editProducts)
    .delete('/:id', deleteProducts)
    
module.exports = Route;